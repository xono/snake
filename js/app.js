// SNAKE
/*
  Elements on the game.
  Board
    color: black
  Prey
    color: silver, gray, red, maroon, yellow, olive, lime, green, aqua, teal, blue, navy, fucsia, purple
  Snake
    color: white
  Score
*/

// Colors.
var colors = ["silver", "gray", "red", "maroon", "yellow", "olive", "lime", "green", "aqua", "teal", "blue", "navy", "fuchsia", "purple"],

// Get a random number between 0 and the limit.
getRandom = function (limit) {
  // Return the number.
  return Math.floor( ( Math.random() * limit ) + 1 );
},

// Default settings for the board.
configureBoard = function (board, color) {
  // Set size and colors.
  board.parentElement.style.height = "100%";
  board.parentElement.style.width = "100%";
  board.style.height = "100%";
  board.style.width = "100%";
  board.style.margin = "0px";
  board.style.padding = "0px";
  board.style.backgroundColor = color;
},

// Create a DOM element.
createElement = function () {
  // Return a div.
  return document.createElement("div");
},

// Create a collection of elements.
/*createElements = function (quantity) {
  // Elements stack.
  var elements = [],
  // Position.
  index = 0;

  // Iterate every element.
  while ( index < quantity ) {
    // Add the element to the stack.
    elements.push(createElement());

    // Next position.
    index++;
  }

  // Return the elements.
  return elements;
},*/

// Configure size, position and color of element. 
configureElement = function (element, color, size, top, left) {
  element.style.backgroundColor = color;
  element.style.height = size + "px";
  element.style.width = size + "px";
  element.style.position = "absolute";
  element.style.top = top + "px";
  element.style.left = left + "px";
},

// Configure a collection of elements.
/*configureElements = function (elements, size, board) {
  // Iterate every element.
  for ( index in elements ) {
    // Set size, position and color.
    configureElement(
      elements[index], 
      colors[getRandom(colors.length - 1)],
      size,
      getRandom(board.offsetHeight - size),
      getRandom(board.offsetWidth - size)
    );
  }
},*/

// Add element to board.
attachElement = function (element, board) {
  // Attach the element.
  board.appendChild(element);
},

// Add a collection of elements to the board.
/*attachElements = function (elements, board) {
  // Iterate every element.
  for ( index in elements ) {
    // Add element.
    attachElement(elements[index]);
  }
},*/

// Create a prey.
createPrey = function (size, board) {
  // Create element.
  var element = createElement();

  // Configure.
  configureElement(
    element, 
    colors[getRandom(colors.length - 1)],
    size,
    getRandom(board.offsetHeight - size),
    getRandom(board.offsetWidth - size)
  );

  // Add.
  attachElement(element, board);

  // Return element as prey.
  return element;
},

// Create a collection of preys.
createPreys = function (quantity, size, board) {
  // Preys stack.
  var preys = [],
  // Position.
  index = 0;

  // Iterate every prey.
  while ( index < quantity ) {
    // Add the prey to the stack.
    preys.push(createPrey(size, board));

    // Next position.
    index++;
  }

  // Return the preys.
  return preys;
};

//
createPreyWorker = function (prey, step, board, snake) {
  // Create worker.
  var worker = new Worker("./js/prey-worker.js");

  // Listen worker events.
  worker.addEventListener("message", function(event) {
    // Move Prey event.
    if ( event.data.command === "move:prey" ) {
      // Update prey position.
      prey.style.top = event.data.top + "px";
      prey.style.left = event.data.left + "px";

      //
      if ( 
        (
          prey.offsetTop <= snake.offsetTop && 
          snake.offsetTop <= ( prey.offsetTop + prey.offsetHeight ) 
        ) &&
        (
          prey.offsetLeft <= snake.offsetLeft &&
          snake.offsetLeft <= ( prey.offsetLeft + prey.offsetWidth ) 
        )
      ) {
        //
        prey.remove();
        //
        worker.terminate();
      } else {
        // Generate next position.
        worker.postMessage({
          command: "position:prey",
          step: step,
          top: prey.offsetTop,
          left: prey.offsetLeft,
          boardHeight: board.offsetHeight,
          boardWidth: board.offsetWidth,
          direction: event.data.direction,
          time: event.data.time
        }); 
      }
    }
  }, false);

  // Start the worker work.
  worker.postMessage({
    command: "position:prey",
    step: step,
    top: prey.offsetTop,
    left: prey.offsetLeft,
    boardHeight: board.offsetHeight,
    boardWidth: board.offsetWidth,
    direction: 0,
    time: 250
  });

  //
  return worker;
},

// Create a collection of workers for preys.
createPreyWorkers = function (preys, step, board, snake, size) {
  // Prey workers stack.
  var workers = [];

  /*var test = function (prey) {
    // Worker for the prey.
    var worker = new Worker("./js/prey-worker.js");

    // Listen worker events.
    worker.addEventListener("message", function(e) {
      // Move Prey event.
      if ( e.data.command === "move:prey" ) {
        // Update prey position.
        prey.style.top = e.data.top + "px";
        prey.style.left = e.data.left + "px";

        // Generate next position.
        worker.postMessage({
          command: "position:prey",
          step: stepSize,
          top: prey.offsetTop,
          left: prey.offsetLeft,
          windowHeight: board.offsetHeight,
          windowWidth: board.offsetWidth,
          direction: e.data.direction
        });
      }
    }, false);

    // Add to stack.
    workers.push(worker);

    // Start the worker work.
    worker.postMessage({
      command: "position:prey",
      step: stepSize,
      top: prey.offsetTop,
      left: prey.offsetLeft,
      windowHeight: board.offsetHeight,
      windowWidth: board.offsetWidth,
      direction: 0
    });
  };*/

  // Iterate every prey position.
  for ( index in preys ) {
    // Create a worker.
    //test(preys[index]);
    workers.push(createPreyWorker(preys[index], step, board, snake));
  }
},

//
( function () {

  // Game board.
  var board = document.getElementById("body");

  // Initialize the board for the game.
  configureBoard(board, "black");


  // 
  var snake = createElement();
  //
  configureElement(
    snake, 
    "white",
    20,
    parseInt(board.offsetHeight / 2, 10),
    parseInt(board.offsetWidth / 2, 10)
  );
  //
  attachElement(snake, board);


  document.addEventListener("keydown", function(event) {
    var top = snake.offsetTop,
    left = snake.offsetLeft;

    //
    if ( event.which === 37 ) {
      left = snake.offsetLeft - 20;
    } else if ( event.which === 38 ) {
      top = snake.offsetTop - 20;
    } else if ( event.which === 39 ) {
      left = snake.offsetLeft + 20;
    } else if ( event.which === 40 ) {
      top = snake.offsetTop + 20;
    }

    // Adjust the top on the board limits.
    if ( top < 0 ) { 
      // Repositionate to minimum top position.
      top = 0; 
    }
    if ( top > ( board.offsetHeight - snake.offsetHeight ) ) { 
      // Repositionate to maximun top position.
      top = board.offsetHeight - snake.offsetHeight; 
    }

    // Adjust the left on the board limits.
    if ( left < 0 ) { 
      // Repositionate to minimum left position.
      left = 0; 
    }
    if ( left > ( board.offsetWidth - snake.offsetWidth ) ) { 
      // Repositionate to maximun left position.
      left = board.offsetWidth - snake.offsetWidth; 
    }

    snake.style.left = left + "px";
    snake.style.top = top + "px";

  });


  // Create the preys.
  var preys = createPreys(100, 20, board);
  //
  var preyWorkers = createPreyWorkers(preys, 20, board, snake, 20);

}) ();




