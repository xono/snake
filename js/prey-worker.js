// Calculate new position for the prey.
var calculatePreyPosition = function (event) {
  // Direction of the prey movement.
  var direction = event.data.direction, 
  // Current top position.
  top = event.data.top,
  // Current left position.
  left = event.data.left,
  // Movement increment unit.
  step = event.data.step,
  // Board sizevent.
  boardHeight = event.data.boardHeight,
  boardWidth = event.data.boardWidth,
  // Opposite direction of the prey movement.
  oppositeDirection;

  /*
    Left and top units on the board.
    0,0 --------------------- l,0
    |                           |
    |                           |
    |                           |
    |                           |
    |                           |
    0,t --------------------- l,t

    Directions of movement.
      N        0       ^
    E   W    2   3   <   >
      S        1.      v
  */

  // Calculate the opposite direction.
  // Move to north.
  if ( direction === 0 ) { 
    // Opposite to south.
    oppositeDirection = 1; 
  }
  // Move to south.
  else if ( direction === 1 ) { 
    // Opposite to north.
    oppositeDirection = 0; 
  }
  // Move to east.
  else if ( direction === 2 ) { 
    // Opposite to west.
    oppositeDirection = 3; 
  }
  // Move to west.
  else if ( direction === 3 ) { 
    // Opposite to east.
    oppositeDirection = 2; 
  }

  // Get new direction while not be opposite direction.
  do {
    direction = parseInt(( Math.random() * 4), 10);
  }
  while ( direction === oppositeDirection )

  // Calculate the new top or left position according to the new direction.
  // North.
  if ( direction === 0 ) {
    // Increment top.
    top = top + step;
  // South.
  } else if ( direction === 1 ) {
    // Decrement top.
    top = top - step;
  // East.
  } else if ( direction === 2 ) {
    // Decrement left.
    left = left - step;
  // West
  } else if ( direction === 3 ) {
    // Increment left.
    left = left + step;
  }

  // Adjust the top on the board limits.
  if ( top < 0 ) { 
    // Repositionate to minimum top position.
    top = 0; 
  }
  if ( top > ( boardHeight - step ) ) { 
    // Repositionate to maximun top position.
    top = boardHeight - step; 
  }

  // Adjust the left on the board limits.
  if ( left < 0 ) { 
    // Repositionate to minimum left position.
    left = 0; 
  }
  if ( left > ( boardWidth - step ) ) { 
    // Repositionate to maximun left position.
    left = boardWidth - step; 
  }

  // Return the new position.
  return {
    top: top,
    left: left,
    direction: direction
  };
};

// Listen events on the worker.
self.addEventListener("message", function(event) {

  // Filter events by command.
  if ( event.data.command === "position:prey" ) {

    // Wait a 1/4 second to move again.
    setTimeout( function () {
      // Get a new position.
      var position = calculatePreyPosition(event);

      // Move the prey.
      self.postMessage({
        command: "move:prey",
        top: position.top,
        left: position.left,
        direction: position.direction,
        time: event.data.time
      });
    }, event.data.time);

  }

}, false);
